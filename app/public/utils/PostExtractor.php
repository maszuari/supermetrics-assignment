<?php
namespace utils;

class PostExtractor {
    
    static public function getMonthYear($datetime){
        $date = substr($datetime, 0, 10);
        $mon = date('m', strtotime($date));
        $year = date('Y', strtotime($date));
        return "$mon-$year";
    }

    static public function getWeekNumber($datetime){
        $date = substr($datetime, 0, 10);
        $weekNumber = strftime('%-V',strtotime($date));
        return $weekNumber;
    }
}