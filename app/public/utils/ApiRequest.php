<?php
namespace utils;

class ApiRequest {

    public function get($url){

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HTTPGET, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response_json = curl_exec($ch);
        curl_close($ch);
        $response=json_decode($response_json);
        return $response;
    }

    public function post($url, $postData){
        $options = array(
            'http' => array(
              'method'  => 'POST',
              'content' => json_encode( $postData ),
              'header'=>  "Content-Type: application/json\r\n" .
                          "Accept: application/json\r\n"
              )
            );
        $context = stream_context_create( $options );
        $result = file_get_contents( $url, false, $context );
        $response = json_decode( $result );
        return $response;
    }
}