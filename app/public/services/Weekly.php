<?php

class Weekly {
    
    public $week_number;
    public $total_posts;

    public function __construct(){
        $this->total_posts = 0;
    }

    public function addPost(){
        $this->total_posts = $this->total_posts + 1;
    }
}