<?php

class Monthly {

    public $name;
    private $total_char_length;
    private $total_posts;
    private $longest_char_length;
    private $users;
    private $average_char_length;
    private $average_posts_per_user;

    public function __construct() {
        $this->total_posts = 0;
        $this->total_char_length = 0;
        $this->average_char_length = 0;
        $this->longest_char_length = 0;
        $this->users = array();
    }

    public function setCharLength($length){
        $this->total_char_length = $this->total_char_length + $length;
        $this->total_posts = $this->total_posts + 1;

        if( $length >= $this->longest_char_length ){
            $this->longest_char_length = $length;
        }
    }

    public function setUser($userId){
        if( !in_array($userId, $this->users)) {
            array_push($this->users, $userId);
        }
    }

    public function getTotalUsers(){
        return count($this->users);
    }

    public function getLongestPost(){
        return $this->longest_char_length;
    }

    public function getAverageCharLength(){
        $this->average_char_length = round($this->total_char_length / $this->total_posts);
        return $this->average_char_length;
    }

    public function getAveragePostsPerUser(){
        $this->average_posts_per_user = round($this->total_posts / (count($this->users)));
        return $this->average_posts_per_user;
    }

}