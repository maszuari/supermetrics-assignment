<?php
namespace services;
require './utils/ApiRequest.php';

use utils\ApiRequest;
$apiRequest =  new ApiRequest();

class DataService {

    public $hasError = true;
    public $errorMsg = '';

    public function getErrorMsg(){
        return $this->errorMsg;
    }

    public function getPosts($slToken, $page) {
        $URL_FETCH = "https://api.supermetrics.com/assignment/posts?sl_token=$slToken&page=$page";
        $apiRequest =  new ApiRequest();
        $result = $apiRequest->get($URL_FETCH);
        
        if( isset($result->error) ){
            $this->hasError = true;
            $this->errorMsg = $result->error->message;
            return null;
        }else {
            $this->hasError = false;
            return $result->data->posts;
        }
    }
    
    public function getToken($postData){
        $URL_REGISTER = "https://api.supermetrics.com/assignment/register";
        $apiRequest =  new ApiRequest();
        $result = $apiRequest->post($URL_REGISTER, $postData);
        if( array_key_exists('data', $result)){
            $this->hasError = false;
            return $result->data->sl_token;
        }else{
            return null;
        }
    }
}