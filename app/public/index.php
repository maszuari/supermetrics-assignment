<?php
require './services/DataService.php';
require './services/Monthly.php';
require './services/Weekly.php';
require './utils/PostExtractor.php';

use services\DataService;
use utils\PostExtractor;

header('Content-Type: application/json');

$postData = array(
    "client_id"=>"ju16a6m81mhid5ue1z3v2g0uh",
    "email"=>"maszuari@outlook.com",
    "name"=>"Maszuari"
);

$ds = new DataService();
$token = $ds->getToken($postData);
if( $token ){
    $months = array();
    $weeks = array();
    for ($page=1; $page<=10; $page++){
        $posts = $ds->getPosts($token, $page);
        if( $posts != null ){

            foreach( $posts as $post){
  
                $mon = PostExtractor::getMonthYear($post->created_time);
               
                if (array_key_exists($mon,$months)) {
                    
                    $currentMon = $months[$mon];
                    $currentMon->setCharLength(strlen($post->message));
                    $currentMon->setUser($post->from_id);

                }else{
                    $monthly = new Monthly();
                    $monthly->name = $mon;
                    $monthly->setCharLength(strlen($post->message));
                    $monthly->setUser($post->from_id);
                    $months[$mon] = $monthly;
                }

                $wek = PostExtractor::getWeekNumber($post->created_time);
                if (array_key_exists($wek, $weeks)) {
                    $currentWeek = $weeks[$wek];
                    $currentWeek->addPost();
                }else{
                    $weekly = new Weekly();
                    $weekly->week_number = $wek;
                    $weekly->addPost();
                    $weeks[$wek] = $weekly;
                }

            }
        }else {
            echo json_encode(array(
                'status_code'=>500,
                'error_message'=>$ds->getErrorMsg()
            ));
        }
    }
    ksort($months);
    $summary = array();
    foreach($months as $item) {
        $obj = array(
            'month'=> $item->name,
            'average_char_length'=>$item->getAverageCharLength(),
            'longest_post'=> $item->getLongestPost(),
            'average_posts_per_user'=> $item->getAveragePostsPerUser()
        );
        $summary['months'][] = $obj;
    }

    ksort($weeks);
    foreach ($weeks as $wk) {
        $summary['week_numbers'][] = $wk;
    }
    echo json_encode($summary);

}else {
    echo json_encode(array(
        'status_code'=>500,
        'error_message'=>'Token is unavailable'
    ));
}