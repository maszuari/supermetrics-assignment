Prerequisites:-

1. Docker. 
2. Docker Compose.

Installation:-
1. Install Docker. You can find instructions for the installation here; https://docs.docker.com/install/ .
2. Install Docker Compose. You can find instructions for the installation here; https://docs.docker.com/compose/install/.
3. On your terminal: git clone https://gitlab.com/maszuari/supermetrics-assignment.git .
4. Run this command: docker-compose up

To view the results:
- Go to http://127.0.0.1:9090/index.php .